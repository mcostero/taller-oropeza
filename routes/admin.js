const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const nodemailer = require('nodemailer')
const mysql = require('mysql')
const session = require('express-session')
const app = express()

//conexion a mysql
var host_mysql, user_mysql, password_mysql, database_mysql

host_mysql = process.env.OPENSHIFT_MYSQL_DB_HOST || 'localhost'
user_mysql = process.env.OPENSHIFT_MYSQL_DB_USERNAME || 'root'
password_mysql = process.env.OPENSHIFT_MYSQL_DB_PASSWORD || ''
database_mysql = process.env.OPENSHIFT_APP_NAME || 'taller'

var con = mysql.createConnection({
  host: host_mysql,
  user: user_mysql,
  password: password_mysql,
  database: database_mysql
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride('X-HTTP-Method-Override'))
app.use(session({
  secret: 'laclave',
  resave: false,
  saveUninitialized: true
}))

app.use(function(req, res, next) {
  console.log('REQUEST: %s', Date(Date.now()))
  next()
})

app.get('/', function(req, res) {
  res.render('admin/index')
})

app.post('/login', function (req, res) {
  if(!req.body) return res.send("No se ha ingresado usuario o contraseña")
  var views = req.session.views  

  let user = req.body.usuario, password = req.body.password

  if (views === true) {
    res.redirect('/administracion/dashboard')
  } else {
    if (req.body.usuario === 'admin' && req.body.password == 'admin') {
      views = req.session.views = true

      res.redirect('/administracion/dashboard')
    } else {
      res.redirect('/administracion/')
    }
  }
})

app.get('/logout', function (req, res) {
  var views = req.session.views

  if (view === true) {
    views = req.session.views = false
    res.redirect('/administracion')
  } else {
    res.redirect('/administracion')
  }
})

app.get('/dashboard', function (req, res) {
  var views = req.session.views
  if (views === true) {
    con.query('SELECT * FROM inscripciones', function (err, rows) {
      if(err) throw err

      res.render('admin/dashboard', {inscritos: rows})
    })
  } else {
    res.redirect('/administracion/')
  }
})

app.get('/eliminar/:id_ins', function (req, res) {
  var views = req.session.views
  if (views === true) {
    if (!req.params.id_ins) return res.send("No se ha proporcionado un id válido")

    con.query('DELETE FROM inscripciones WHERE id = " '+ req.params.id_ins +' "', function (err, rows) {
      if(err) throw err

      res.redirect('/administracion/dashboard')
    })
  } else {
    res.redirect('/administracion/')
  }
})

app.get('/modificar/:id_ins', function (req, res) {
  var views = req.session.views
  if (views === true) {
    if (!req.params.id_ins) return res.send("No se ha proporcionado un id válido")

    con.query('SELECT * FROM inscripciones WHERE id = " ' + req.params.id_ins + ' "', function (err, rows) {
      if (err) throw err

      res.render('admin/edit', {inscripcion: rows[0]})
    })
  } else {
    res.redirect('/administracion/')
  }
})

app.post('/modificar/:id_ins', function (req, res) {
  var views = req.session.views
  if (views === true) {
    if (!req.params.id_ins) return res.send("No se ha proporcionado un id válido")
    if (!req.body) return res.send("No se ha completado el formulario")

    con.query('UPDATE inscripciones SET nombre = "'+req.body.nombre+'",apellido = "'+req.body.apellido+'",email = "'+req.body.email+'",cedula = "'+req.body.cedula+'",telefono = "'+req.body.telefono+'",titulo = "'+req.body.titulo+'",institucion = "'+req.body.institucion+'",cargo = "'+req.body.cargo+'" WHERE id = " ' + req.params.id_ins + ' "', function (err, rows) {
      if (err) throw err

      res.redirect('/administracion/dashboard')
    })
  } else {
    res.redirect('/administracion/')
  }
})

module.exports = app
