const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const nodemailer = require('nodemailer')
const mysql = require('mysql')
const app = express()

//conexion a mysql
var host_mysql, user_mysql, password_mysql, database_mysql

host_mysql = process.env.OPENSHIFT_MYSQL_DB_HOST || 'localhost'
user_mysql = process.env.OPENSHIFT_MYSQL_DB_USERNAME || 'root'
password_mysql = process.env.OPENSHIFT_MYSQL_DB_PASSWORD || ''
database_mysql = process.env.OPENSHIFT_APP_NAME || 'taller'

var con = mysql.createConnection({
  host: host_mysql,
  user: user_mysql,
  password: password_mysql,
  database: database_mysql
})

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride('X-HTTP-Method-Override'))

app.use(function(req, res, next) {
  console.log('REQUEST: %s', Date(Date.now()))
  next()
})

app.get('/', function (req, res) {
  res.render('index', {titulo: "I Seminario Educativo | Colectivo de Directores del Circuito Educativo I"})
})

app.get('/inscripciones', function (req, res) {
  res.render('form', {titulo: "I Seminario Educativo | Colectivo de Directores del Circuito Educativo I"})
})

app.post('/inscripciones', function (req, res) {
  if (!req.body) return res.status(400).json({"msg":"Error"})

  con.query('INSERT INTO inscripciones VALUES (DEFAULT, NOW(), "'+req.body.nombre+'","'+req.body.apellido+'","'+req.body.email+'","'+req.body.cedula+'","'+req.body.telefono+'","'+req.body.instruccion+'","'+req.body.nombre_institucion+'","'+req.body.cargo+'")', function (err, rows) {
    if(err) throw err

    // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport('smtps://circuito.educativo.1%40gmail.com:123456circuito@smtp.gmail.com')

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"Colectivo de Directores del Circuito Educativo I 👥" <circuito.educativo.1@gmail.com>',
        to: req.body.email + ', circuito.educativo.1@gmail.com', // list of receivers
        subject: 'Incripción al I Seminario Educativo por Colectivo de Directores del Circuito Educativo I', // Subject line
        html: '<h2>Hola '+ req.body.nombre +' '+ req.body.apellido +', su inscripción ha sido formalizada.</h2><br><p>Sus datos ingresados son: <br> Nombre: '+req.body.nombre+'<br> Apellido: '+req.body.apellido+'<br> Cedula: '+req.body.cedula+'<br> Telefono: '+req.body.telefono+'<br> Correo Electrónico: '+req.body.email+'<br> Nombre de la institución: '+req.body.nombre_institucion+'<br> Cargo: '+req.body.cargo+'<br> Grado de instrucción: '+req.body.instruccion+'</p><p>El Colectivo de Directores del Circuito Educativo 1 Invitan al 1er. Seminario Educativo</p><br><p><b>EL DOCENTE EN FUNCIÓN DIRECTIVA COMO LÍDER PROTAGÓNICO Y PARTICIPATIVO  PARA LA TRANSFORMACIÓN SOCIAL Y EDUCATIVA DE LAS POLITICAS PUBLICAS</b></p><br><p>Miércoles 29 de Junio (29/06/16), Hora 8:00 am a 2:00 pm</p><p>Lugar: Colegio "Antonio Rosmini".</p>'
    }

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        if(error) return console.log(error)

        console.log('Message sent: ' + info.response)
        res.redirect('/inscripciones')
    })
  })
})

module.exports = app
