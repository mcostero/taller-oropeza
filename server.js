'use strict'

const express = require('express')
const app = express()
const routes = require('./routes/main')
const admin = require('./routes/admin')

//IP  y puerto de la app
var ipaddress, port
ipaddress = process.env.OPENSHIFT_NODEJS_IP || process.env.OPENSHIFT_INTERNAL_IP
port      = process.env.OPENSHIFT_NODEJS_PORT || process.env.OPENSHIFT_INTERNAL_PORT || 8080

if (typeof ipaddress === "undefined") {
    console.warn('No OPENSHIFT_*_IP var, using 127.0.0.1');
    ipaddress = "127.0.0.1";
}

//Motor de plantillas
app.set('view engine', 'pug')

//Archivos estaticos
app.use('/public', express.static('assets'))
app.use('/public', express.static('bower_components'))

//rutas
app.use('/', routes)
app.use('/administracion', admin)

//ejecucion del server
app.listen(port, ipaddress, function () {
	console.log('%s: Node server started on %s:%d ...', Date(Date.now() ), ipaddress, port)
})
